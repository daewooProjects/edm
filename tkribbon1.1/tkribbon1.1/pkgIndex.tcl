namespace eval ::tkribbon {
  if {[catch {package require platform}]} {
    ## Set platform variable to "win32-x86_64" if using 64-bit Tcl.
    variable platform win32-ix86
  } else {
    variable platform [::platform::generic]
  }
  variable version 1.1
};# namespace tkribbon

package ifneeded tkribbon 1.1 \
  [list load [file join $dir $::tkribbon::platform libtkribbon1.1[info sharedlibextension]] tkribbon]