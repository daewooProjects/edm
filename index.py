from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import functools
from collections import Counter
from docutils.nodes import row


def createExamples():
    numberArrayExamples = open('number', 'a')
    numbersWeHave = range(0, 10)
    versionsWeHave = range(1, 10)
    for eachNumber in numbersWeHave:
        for eachVersion in versionsWeHave:
            print(str(eachNumber) + '.' + str(eachVersion))
            imgFilePath = 'images/numbers/' + str(eachNumber) + '.' + str(eachVersion) + '.png'
            ei = Image.open(imgFilePath)
            temp = np.array(ei)
            eiar = temp.copy()
            eiar1 = str(eiar.tolist())
            lineToWrite = str(eachNumber) + '::' + eiar1 + '\n'
            numberArrayExamples.write(lineToWrite)


def whatNumIsThis(filePath):
    matchedAr = []
    loadExamps = open('number', 'r').read()
    loadExamps = loadExamps.split('\n')
    print(len(loadExamps))
    i = Image.open(filePath)
    temp = np.array(i)
    iar = temp.copy()
    iarl = iar.tolist()

    inQuestion = str(iarl)
    print('in question: ', inQuestion)

    for eachExamp in loadExamps:
        if len(eachExamp) > 3:
            splitEx = eachExamp.split('::')
            currentNum = splitEx[0]
            currentAr = splitEx[1]

            eachPixEx = currentAr.split('], ')
            eachPixInQuestion = inQuestion.split('], ')
            #print(eachPixEx, '\n')
            x = 0
            while x < len(eachPixEx):
                if eachPixEx[x] == eachPixInQuestion[x]:
                    matchedAr.append(int(currentNum))
                x += 1
            print('matched array: ', matchedAr)

    print(matchedAr)
    x = Counter(matchedAr)
    print(x)

    graphX = []
    graphY = []

    for eachThing in x:
        print(eachThing)
        graphX.append(eachThing)
        print(x[eachThing])
        graphY.append(x[eachThing])
    fig = plt.figure()
    ax1 = plt.subplot2grid((4, 4), (0, 0), rowspan=1, colspan=4)
    ax2 = plt.subplot2grid((4, 4), (1, 0), rowspan=3, colspan=4)
    ax1.imshow(temp)
    ax2.bar(graphX, graphY, align='center')
    plt.ylim(400)
    xLoc = plt.MaxNLocator(12)
    ax2.xaxis.set_major_locator(xLoc)

    plt.show()


# make image black or white
def threshold(imageArray):
    print(imageArray)
    balacneArray = []
    newAr = imageArray
    # newAr.setflags(write=1)
    print(newAr.flags)
    for eachRow in imageArray:
        for eachPixel in eachRow:
            avrNum = functools.reduce(lambda x, y: x + y, eachPixel[:3]) / len(eachPixel[:3])
            balacneArray.append(avrNum)
    balance = functools.reduce(lambda x, y: x + y, balacneArray) / len(balacneArray)
    for eachRow in newAr:
        for eachPixel in eachRow:
            if functools.reduce(lambda x, y: x + y, eachPixel[:3]) / len(eachPixel[:3]) > balance:
                eachPixel[0] = 255
                eachPixel[1] = 255
                eachPixel[2] = 255
                eachPixel[3] = 255
            else:
                eachPixel[0] = 0
                eachPixel[1] = 0
                eachPixel[2] = 0
                eachPixel[3] = 255
    return newAr


whatNumIsThis('images/test.png')

# i = Image.open('images/numbers/0.1.png')
# iar_1 = np.asarray(i)
# iar = iar_1.copy()
# #iar.setflags(write=1)
#
#
# i2 = Image.open('images/numbers/y0.4.png')
# iar2_1 = np.asarray(i2)
# iar2 = iar2_1.copy()
# i3 = Image.open('images/numbers/y0.5.png')
# iar3_1 = np.asarray(i3)
# iar3 = iar3_1.copy()
#
# i4 = Image.open('images/sentdex.png')
# iar4_1 = np.asarray(i4)
# iar4 = iar4_1.copy()
#
# createExamples()

'''threshold(iar2)
threshold(iar3)
threshold(iar4)

fig = plt.figure()
ax1 = plt.subplot2grid((8, 6), (0, 0), rowspan=4, colspan=3)
ax2 = plt.subplot2grid((8, 6), (4, 0), rowspan=4, colspan=3)
ax3 = plt.subplot2grid((8, 6), (0, 3), rowspan=4, colspan=3)
ax4 = plt.subplot2grid((8, 6), (4, 3), rowspan=4, colspan=3)

ax1.imshow(iar)
ax2.imshow(iar2)
ax3.imshow(iar3)
ax4.imshow(iar4)

plt.show()

# print(iar)
# plt.imshow(iar)
# plt.show()'''
