import tkinter as tk

from tkinter import *
#from tkinter import Widget
from os import path

#Ribbon menu
# class Ribbon(Widget):
#     def __init__(self, master, kw=None):
#         self.version = master.tk.call('package','require','tkribbon')
#         self.library = master.tk.eval('set ::tkribbon::library')
#         Widget.__init__(self, master, 'tkribbon::ribbon', kw=kw)
#
#     def load_resource(self, resource_file, resource_name='APPLICATION_RIBBON'):
#         """Load the ribbon definition from resources.
#
#         Ribbon markup is compiled using the uicc compiler and the resource included
#         in a dll. Load from the provided file."""
#         self.tk.call(self._w, 'load_resources', resource_file)
#         self.tk.call(self._w, 'load_ui', resource_file, resource_name)

def exit():
    root.destroy()

# def getResourse():
#     if len(sys.argv) > 1:
#         resource = sys.argv[1]
#         if len(sys.argv) > 2:
#             name = sys.argv[2]
#     else:
#         resource = path.join(ribbon.library, 'libtkribbon1.0.dll')
#     return  resource
root = tk.Tk()
#size
RWidth=root.winfo_screenwidth()
RHeight=root.winfo_screenheight()
root.geometry(("%dx%d")%(RWidth*0.6,RHeight*0.6))
#ribbon
# ribbon = Ribbon(root)
# ribbon.grid(row=0, column=0)
# name = 'APPLICATION_RIBBON'
#
# ribbon.load_resource(getResourse(), name)
# #t = Text(root)
# ribbon.grid(sticky=(N, E, S, W))
# #t.grid(sticky=(N, E, S, W))
# root.grid_columnconfigure(0, weight=1)
# root.grid_rowconfigure(1, weight=1)


# Main menu
mainMenu = tk.Menu(root)

root.config(menu=mainMenu)
#Program submenu
programMenu = tk.Menu(mainMenu, tearoff=0)
programMenu.add_command(label='Options')
programMenu.add_command(label='About')
programMenu.add_command(label='Exit', command=exit)
#Docs submenu
docsMenu = tk.Menu(mainMenu, tearoff=0)
docsMenu.add_command(label='New')
docsMenu.add_command(label='Modify')
docsMenu.add_command(label='Delete')
#View submenu
viewMenu = tk.Menu(mainMenu, tearoff=0)
viewMenu.add_command(label='In')
viewMenu.add_command(label='Out')
viewMenu.add_command(label='Enquiry')
#Operations submenu
operationsMenu = tk.Menu(mainMenu, tearoff=0)
operationsMenu.add_command(label='Import')
operationsMenu.add_command(label='Export')
#Help submenu
helpMenu = tk.Menu(mainMenu, tearoff=0)
helpMenu.add_command(label='Help')

mainMenu.add_cascade(label="Programm", menu=programMenu)
mainMenu.add_cascade(label="Document", menu=docsMenu)
mainMenu.add_cascade(label="View", menu=viewMenu)
mainMenu.add_cascade(label="Operations", menu=operationsMenu)
mainMenu.add_cascade(label="Help", menu=helpMenu)



if __name__ == '__main__':
    def main():
        root.mainloop()

main()